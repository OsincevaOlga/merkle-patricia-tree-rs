#![feature(test)]

extern crate test;
extern crate patricia;
extern crate rand;

use rand::Rng;
use std::collections::HashSet;

use test::Bencher;
use patricia::patricia::Patricia;

// Makes large data set with unique keys
fn generate_random_data(len: usize, string_len: usize) -> Vec<(String, u64)> {
    let mut rng = rand::thread_rng();

    let mut exists_keys = HashSet::new();
    let kv_generator = |_| {
        let v = rng.gen::<u64>();
        let len = rng.gen_range(1, string_len);
        let mut gen_key = || rng.gen_ascii_chars().take(len).collect();

        // Generate only unique keys
        let mut s: String = gen_key();
        while exists_keys.contains(&s) {
            s = gen_key();
        }

        exists_keys.insert(s.clone());
        (s, v)
    };

    (0..len)
        .map(kv_generator)
        .collect::<Vec<_>>()
}

#[bench]
fn insert(b: &mut Bencher) {
    b.iter(|| {
        let mut p = Patricia::new();
        p.insert(b"abc", 1);
        p.insert(b"def", 2);
        p.insert(b"qwe", 3);
        p.insert(b"cko", 4);
        p.insert(b"opr", 5);
        p.insert(b"ossse", 6);
        p.insert(b"abd", 10);
    });  
}

#[bench]
fn remove_after_insert(b: &mut Bencher) {  
    b.iter(|| {
        let mut p = Patricia::new();
        p.insert(b"abc", 1);
        p.insert(b"def", 2);
        p.insert(b"qwe", 3);
        p.insert(b"cko", 4);
        p.insert(b"opr", 5);
        p.insert(b"ossse", 6);
        p.insert(b"abd", 10); 
        
        p.remove(b"cko");
        p.remove(b"abc");
        p.remove(b"def");
        p.remove(b"ossse");
        p.remove(b"abd"); 
        p.remove(b"qwe");
        p.remove(b"opr");
    });
}

#[bench]
fn fuzzy_insert_and_remove(b: &mut Bencher) {
    let data = generate_random_data(1000, 32);
    let keys_to_remove = data.iter()
                             .take(100)
                             .map(|item| item.0.clone())
                             .collect::<Vec<_>>();

    b.iter(|| {
        let mut p = Patricia::new();
        for item in &data {
            let bytes = item.0.as_bytes();
            p.insert(bytes, item.1);
        }
        for key in &keys_to_remove {
            p.remove(key.as_bytes());
        }
    })
}

#[bench]
fn fuzzy_insert_and_remove_large_keys(b: &mut Bencher) {
    let data = generate_random_data(10000, 255);
    let keys_to_remove = data.iter()
                             .take(500)
                             .map(|item| item.0.clone())
                             .collect::<Vec<_>>();

    b.iter(|| {
        let mut p = Patricia::new();
        for item in &data {
            let bytes = item.0.as_bytes();
            p.insert(bytes, item.1);
        }
        for key in &keys_to_remove {
            p.remove(key.as_bytes());
        }
    })
}
