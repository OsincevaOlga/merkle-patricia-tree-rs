extern crate bincode;
extern crate rustc_serialize;
extern crate crypto;

use std::collections::BTreeMap;
use std::mem;

use rustc_serialize::Encodable;
use bincode::rustc_serialize::encode;
use bincode::SizeLimit;
use crypto::digest::Digest;
use crypto::sha2::Sha256;

#[derive(Debug)]
pub struct Patricia<V>
    where V: Encodable
{
    value: Option<V>,
    key: Vec<u8>,
    hash: [u8; 32],
    children: BTreeMap<u8, Patricia<V>>,
}

fn max_matched_length(a: &[u8], b: &[u8]) -> usize {
    let mut i: usize = 0;
    while i < a.len() && i < b.len() && a[i] == b[i] {
        i += 1;
    }
    i
}

#[allow(dead_code)]
fn make_str(a: &[u8]) -> &str {
    use std::str;
    str::from_utf8(a).unwrap()
}

impl<V> Patricia<V>
    where V: Encodable
{
    pub fn new() -> Patricia<V> {
        Patricia {
            value: None,
            key: Vec::new(),
            children: BTreeMap::new(),
            hash: [0; 32],
        }
    }

    pub fn get(&self, key: &[u8]) -> Option<&V> {
        if self.key.is_empty() || key.starts_with(&self.key) {
            if key.len() == self.key.len() {
                return self.value.as_ref();
            } else if key.len() > self.key.len() {
                let suffix = &key[self.key.len()..];
                return self.children[&suffix[0]].get(suffix);
            }
        }
        None
    }

    pub fn insert(&mut self, key: &[u8], value: V) {
        if self.is_empty() {
            self.key.extend_from_slice(key);
            self.value = Some(value);
        } else {
            let i = max_matched_length(&self.key, key);
            if i >= self.key.len() {
                if i == key.len() {
                    if self.value.is_some() {
                        panic!("Given key is already exists");
                    }
                    self.value = Some(value);
                    self.update_hash();
                } else {
                    let suffix = &key[i..];
                    self.insert_predecessor(suffix, value);
                }
            } else {
                // Move root value into leaf
                let prefix = self.key[0..i].to_vec();
                let suffix = self.key[i..].to_vec();

                self.key = prefix;
                let mut node = Patricia {
                    key: suffix,
                    value: self.value.take(),
                    children: BTreeMap::new(),
                    hash: [0; 32],
                };
                mem::swap(&mut node.children, &mut self.children);
                node.update_hash();

                self.children.clear();
                self.children.insert(node.key[0], node);

                if i == key.len() {
                    self.value = Some(value)
                } else {
                    self.value = None;
                    let suffix = &key[i..];
                    self.insert_predecessor(suffix, value);
                }
            }
        }
        self.update_hash();
    }

    pub fn remove(&mut self, key: &[u8]) -> Option<V> {
        if self.key.is_empty() || key.starts_with(&self.key) {
            if key.len() == self.key.len() {
                let value = self.value.take();
                self.value = None;
                self.try_to_compress();
                return value;
            } else if key.len() > self.key.len() {
                let suffix = &key[self.key.len()..];
                let mut value = None;
                if let Some(mut node) = self.children.get_mut(&suffix[0]) {
                    value = node.remove(suffix);
                }
                if value.is_some() {
                    if self.children[&suffix[0]].is_empty() {
                        self.children.remove(&suffix[0]);
                    }
                    self.try_to_compress();
                    return value;
                }
            }
        }
        None
    }


    pub fn is_empty(&self) -> bool {
        self.children.is_empty() && self.value.is_none()
    }

    fn insert_predecessor(&mut self, suffix: &[u8], value: V) {
        let child_to_push = match self.children.get_mut(&suffix[0]) {
            Some(pred) => {
                pred.insert(suffix, value);
                None
            }
            None => {
                let mut child = Patricia::new();
                child.insert(suffix, value);
                Some(child)
            }
        };
        if let Some(child) = child_to_push {
            self.children.insert(child.key[0], child);
        }
    }

    fn add_hashed_value(&self, out: &mut Sha256) {
        let input = encode(self.value.as_ref().unwrap(), SizeLimit::Infinite).unwrap();

        let mut hash = [0; 32];
        let mut hasher = Sha256::new();
        hasher.input(&input);
        hasher.result(&mut hash);

        out.input(&hash);
    }

    fn add_hashed_key(&self, out: &mut Sha256) {
        let mut hash = [0; 32];
        let mut hasher = Sha256::new();
        hasher.input(&self.key);
        hasher.result(&mut hash);

        out.input(&hash);
    }

    // Update node hash in order hash(hash(key) + hash(value) + children's hashes)
    fn update_hash(&mut self) {
        if self.is_empty() {
            self.hash = [0; 32];
        } else {
            let mut hasher = Sha256::new();

            self.add_hashed_key(&mut hasher);
            if self.value.is_some() {
                self.add_hashed_value(&mut hasher);
            }
            for child in self.children.values() {
                hasher.input(&child.hash);
            }

            hasher.result(&mut self.hash);
        }
    }

    // Compress child path
    fn try_to_compress(&mut self) {
        if self.is_empty() {
            self.key.clear();
        } else if self.value.is_none() && self.children.len() == 1 {
            // Compress path
            let mut new_children = BTreeMap::new();
            {
                let child;
                {
                    let (_, node) = self.children.iter_mut().next().unwrap();
                    child = node;
                }
                self.key.extend_from_slice(&child.key);
                self.value = child.value.take();
                mem::swap(&mut new_children, &mut child.children);
            }
            mem::swap(&mut self.children, &mut new_children);
        }
        self.update_hash();
    }
}

impl<V> PartialEq for Patricia<V>
    where V: Clone + Encodable
{
    fn eq(&self, other: &Self) -> bool {
        self.hash == other.hash
    }
}

impl<V> Eq for Patricia<V> where V: Clone + Encodable {}

#[cfg(test)]
mod tests {
    extern crate rand;

    use super::*;
    use std::collections::HashSet;
    use crypto::digest::Digest;
    use crypto::sha2::Sha256;
    use rand::Rng;
    use rustc_serialize::Encodable;

    pub fn validate_patricia<V: Clone + Encodable>(patricia: &Patricia<V>) {
        if patricia.is_empty() {
            assert!(patricia.key.is_empty());
        }
    }

    // Makes large data set with unique keys
    fn generate_random_data(len: usize, string_len: usize) -> Vec<(Vec<u8>, u64)> {
        let mut rng = rand::thread_rng();

        let mut exists_keys = HashSet::new();
        let kv_generator = |_| {
            let v = rng.gen::<u64>();
            let len = rng.gen_range(1, string_len);

            // Generate only unique keys
            let mut s: Vec<u8> = Vec::new();
            s.resize(len, 0);
            rng.fill_bytes(&mut s);
            while exists_keys.contains(&s) {
                rng.fill_bytes(&mut s);
            }
            exists_keys.insert(s.clone());
            (s, v)
        };

        (0..len)
            .map(kv_generator)
            .collect::<Vec<_>>()
    }

    #[test]
    fn max_matched_length() {
        use super::max_matched_length;
        assert_eq!(max_matched_length(b"", b""), 0);
        assert_eq!(max_matched_length(b"a", b""), 0);
        assert_eq!(max_matched_length(b"a", b"b"), 0);
        assert_eq!(max_matched_length(b"a", b"a"), 1);
        assert_eq!(max_matched_length(b"abac", b"abad"), 3);
        assert_eq!(max_matched_length(b"aba", b"abacaba"), 3);
        assert_eq!(max_matched_length(b"abacaba", b"abac"), 4);
    }

    #[test]
    fn insert_suffix() {
        let mut patricia = Patricia::new();
        patricia.insert(b"", 0);
        patricia.insert(b"aba", 1);
        patricia.insert(b"abac", 2);
        patricia.insert(b"abaca", 3);
        patricia.insert(b"abacaba", 4);

        println!("{:#?}", patricia);

        assert_eq!(patricia.get(b""), Some(&0));
        assert_eq!(patricia.get(b"aba"), Some(&1));
        assert_eq!(patricia.get(b"abac"), Some(&2));
        assert_eq!(patricia.get(b"abaca"), Some(&3));
        assert_eq!(patricia.get(b"abacaba"), Some(&4));
    }

    #[test]
    fn insert_simple() {
        let mut patricia = Patricia::new();
        patricia.insert(b"", 0);
        patricia.insert(b"aba", 1);
        patricia.insert(b"abac", 2);
        patricia.insert(b"abad", 3);
        patricia.insert(b"abacaba", 4);

        println!("{:#?}", patricia);

        assert_eq!(patricia.get(b""), Some(&0));
        assert_eq!(patricia.get(b"aba"), Some(&1));
        assert_eq!(patricia.get(b"abac"), Some(&2));
        assert_eq!(patricia.get(b"abad"), Some(&3));
        assert_eq!(patricia.get(b"abacaba"), Some(&4));
    }

    #[test]
    fn insert_simple_reverse() {
        let mut patricia = Patricia::new();
        patricia.insert(b"abacaba", 4);
        patricia.insert(b"abad", 3);
        patricia.insert(b"abac", 2);
        patricia.insert(b"aba", 1);
        patricia.insert(b"", 0);

        assert_eq!(patricia.get(b""), Some(&0));
        assert_eq!(patricia.get(b"aba"), Some(&1));
        assert_eq!(patricia.get(b"abac"), Some(&2));
        assert_eq!(patricia.get(b"abad"), Some(&3));
        assert_eq!(patricia.get(b"abacaba"), Some(&4));
    }

    #[test]
    fn insert_with_moved_root() {
        let mut patricia = Patricia::new();
        patricia.insert(b"aba", 1);
        patricia.insert(b"abac", 2);
        patricia.insert(b"ab", 3);
        patricia.insert(b"abad", 4);

        assert_eq!(patricia.get(b"aba").unwrap(), &1);
        assert_eq!(patricia.get(b"abac").unwrap(), &2);
        assert_eq!(patricia.get(b"ab").unwrap(), &3);
        assert_eq!(patricia.get(b"abad").unwrap(), &4);
    }

    #[test]
    fn insert_with_empty_root() {
        let mut patricia = Patricia::new();
        patricia.insert(b"foo", 1);
        patricia.insert(b"bar", 2);
        patricia.insert(b"barcode", 3);
        patricia.insert(b"common", 4);

        assert_eq!(patricia.get(b"foo"), Some(&1));
        assert_eq!(patricia.get(b"bar"), Some(&2));
        assert_eq!(patricia.get(b"barcode"), Some(&3));
        assert_eq!(patricia.get(b"common"), Some(&4));
    }

    #[test]
    fn insert_with_empty_root_2() {
        let mut p1 = Patricia::new();
        p1.insert(b"aba", 1);
        p1.insert(b"abac", 2);
        p1.insert(b"abad", 3);
        p1.insert(b"abacaba", 4);
        p1.insert(b"foo", 5);
        println!("p1: {:?}", p1);

        let mut p2 = Patricia::new();
        p2.insert(b"abacaba", 4);
        p2.insert(b"abac", 2);
        p2.insert(b"aba", 1);
        p2.insert(b"abad", 3);
        p2.insert(b"foo", 5);
        println!("p2: {:?}", p2);

        assert_eq!(&p1.hash, &p2.hash);
    }


    #[test]
    #[should_panic]
    fn insert_exists_value() {
        let mut patricia = Patricia::new();

        patricia.insert(b"foo", 1);
        patricia.insert(b"foo", 2);
    }

    #[test]
    fn insert_empty_key() {
        let mut patricia = Patricia::new();
        patricia.insert(&Vec::new(), 10);
        assert_eq!(patricia.get(b""), Some(&10));

        validate_patricia(&patricia);
    }

    #[test]
    fn remove_trivial() {
        let mut patricia = Patricia::new();
        patricia.insert(b"aba", 1);
        assert_eq!(patricia.remove(b"aba").unwrap(), 1);

        assert!(patricia.is_empty());
        validate_patricia(&patricia);
    }

    #[test]
    fn remove_simple() {
        let mut patricia = Patricia::new();
        patricia.insert(b"aba", 1);
        patricia.insert(b"abac", 2);
        patricia.insert(b"abad", 3);
        patricia.insert(b"abacaba", 4);

        println!("{:#?}", patricia);

        assert_eq!(patricia.remove(b"aba"), Some(1));
        assert_eq!(patricia.remove(b"abac"), Some(2));
        assert_eq!(patricia.remove(b"abad"), Some(3));
        assert_eq!(patricia.remove(b"abacaba"), Some(4));

        assert!(patricia.is_empty());
        validate_patricia(&patricia);
    }

    #[test]
    fn remove_with_moved_root() {
        let mut patricia = Patricia::new();
        patricia.insert(b"aba", 1);
        patricia.insert(b"abac", 2);
        patricia.insert(b"ab", 3);
        patricia.insert(b"abad", 4);

        assert_eq!(patricia.remove(b"aba").unwrap(), 1);
        assert_eq!(patricia.remove(b"abac"), Some(2));
        assert_eq!(patricia.remove(b"ab").unwrap(), 3);
        assert_eq!(patricia.remove(b"abad").unwrap(), 4);

        assert!(patricia.is_empty());
        validate_patricia(&patricia);
    }

    #[test]
    fn remove_with_empty_root() {
        let mut patricia = Patricia::new();
        patricia.insert(b"foo", 1);
        patricia.insert(b"bar", 2);
        patricia.insert(b"barcode", 3);
        patricia.insert(b"common", 4);

        assert_eq!(patricia.remove(b"foo").unwrap(), 1);
        assert_eq!(patricia.remove(b"bar").unwrap(), 2);
        assert_eq!(patricia.remove(b"barcode").unwrap(), 3);
        assert_eq!(patricia.remove(b"common").unwrap(), 4);

        assert!(patricia.is_empty());
        validate_patricia(&patricia);
    }

    #[test]
    fn self_hash() {
        let mut patricia = Patricia::new();
        patricia.insert(b"foo", 1);

        let mut hasher = Sha256::new();
        let mut hashed_key = [0; 32];
        hasher.input(b"foo");
        hasher.result(&mut hashed_key);
        hasher.reset();

        let mut hashed_value = [0; 32];
        hasher.input(&[0, 0, 0, 1]);
        hasher.result(&mut hashed_value);
        hasher.reset();

        let mut data = Vec::new();
        let mut digest = [0; 32];
        data.extend_from_slice(&hashed_key);
        hasher.input(&hashed_key);
        hasher.input(&hashed_value);
        hasher.result(&mut digest);

        assert_eq!(&patricia.hash, &digest);
    }

    #[test]
    fn hash_simple() {
        let mut p1 = Patricia::new();
        p1.insert(b"aba", 1);
        p1.insert(b"abac", 2);
        p1.insert(b"abad", 3);
        p1.insert(b"abacaba", 4);
        p1.insert(b"foo", 5);
        println!("p1: {:?}", p1);

        let mut p2 = Patricia::new();
        p2.insert(b"abacaba", 4);
        p2.insert(b"abac", 2);
        p2.insert(b"aba", 1);
        p2.insert(b"abad", 3);
        p2.insert(b"foo", 5);
        println!("p2: {:?}", p2);

        assert_eq!(&p1.hash, &p2.hash);
    }

    #[test]
    fn hash_moved_root() {
        let mut p1 = Patricia::new();
        p1.insert(b"abc", 1);
        p1.insert(b"def", 2);
        p1.insert(b"qwe", 3);
        p1.insert(b"cko", 4);
        p1.insert(b"opr", 5);
        p1.insert(b"ossse", 6);
        println!("p1: {:?}", p1);

        let mut p2 = Patricia::new();
        p2.insert(b"ossse", 6);
        p2.insert(b"opr", 5);
        p2.insert(b"def", 2);
        p2.insert(b"abc", 1);
        p2.insert(b"cko", 4);
        p2.insert(b"qwe", 3);
        println!("p2: {:?}", p2);

        assert_eq!(&p1.hash, &p2.hash);
    }

    #[test]
    fn hash_different() {
        let mut p1 = Patricia::new();
        p1.insert(b"abc", 1);
        p1.insert(b"def", 2);
        p1.insert(b"qwe", 3);
        p1.insert(b"cko", 4);
        p1.insert(b"opr", 5);
        p1.insert(b"ossse", 6);
        println!("p1: {:?}", p1);

        let mut p2 = Patricia::new();
        p2.insert(b"opr", 5);
        p2.insert(b"def", 2);
        p2.insert(b"abc", 1);
        p2.insert(b"cko", 4);
        p2.insert(b"qwe", 3);
        println!("p2: {:?}", p2);

        assert!(&p1.hash != &p2.hash);
    }

    #[test]
    fn hash_remove_simple() {
        let mut p1 = Patricia::new();
        p1.insert(b"aba", 1);
        p1.insert(b"abac", 2);
        p1.insert(b"abad", 3);
        p1.insert(b"abacaba", 4);
        p1.insert(b"foo", 5);
        println!("p1: {:?}", p1);
        p1.remove(b"aba");
        p1.remove(b"abad");
        p1.remove(b"foo");
        println!("p1 after remove: {:?}", p1);

        let mut p2 = Patricia::new();
        p2.insert(b"abacaba", 4);
        p2.insert(b"abac", 2);
        p2.insert(b"aba", 1);
        p2.insert(b"abad", 3);
        p2.insert(b"foo", 5);
        println!("p2: {:?}", p1);
        p2.remove(b"foo");
        p2.remove(b"aba");
        p2.remove(b"abad");
        println!("p2 after remove: {:?}", p2);

        assert_eq!(&p1.hash, &p2.hash);
    }

    #[test]
    fn hash_remove_complete() {
        let mut patricia = Patricia::new();
        patricia.insert(b"aba", 1);
        patricia.insert(b"abac", 2);
        patricia.insert(b"abad", 3);
        patricia.insert(b"abacaba", 4);

        println!("{:#?}", patricia);

        assert_eq!(patricia.remove(b"aba").unwrap(), 1);
        assert_eq!(patricia.remove(b"abac").unwrap(), 2);
        assert_eq!(patricia.remove(b"abad").unwrap(), 3);
        assert_eq!(patricia.remove(b"abacaba").unwrap(), 4);

        println!("{:#?}", patricia);

        assert_eq!(patricia.hash, [0; 32]);
    }

    #[test]
    fn hash_remove_compress_path() {
        let mut p1 = Patricia::new();
        p1.insert(b"aba", 1);
        p1.insert(b"abac", 2);
        p1.insert(b"abaca", 3);
        p1.insert(b"abacaba", 4);
        p1.insert(b"abad", 5);
        p1.insert(b"abacad", 6);
        println!("p1: {:#?}", p1);
        p1.remove(b"aba"); // 1
        p1.remove(b"abaca"); // 3
        p1.remove(b"abad"); // 5
        p1.remove(b"abacad"); // 6
        println!("p1 after remove: {:#?}", p1);

        let mut p2 = Patricia::new();
        p2.insert(b"abacaba", 4);
        p2.insert(b"abac", 2);
        p2.insert(b"aba", 1);
        p2.insert(b"abaca", 3);
        p2.insert(b"abad", 5);
        p2.insert(b"abacad", 6);
        println!("p2: {:#?}", p1);
        p2.remove(b"abacad"); // 6
        p2.remove(b"abad"); // 5
        p2.remove(b"aba"); // 1
        p2.remove(b"abaca"); //3
        println!("p2 after remove: {:#?}", p2);

        assert_eq!(p2.get(b"abac"), Some(&2));
        assert_eq!(p2.get(b"abacaba"), Some(&4));
        assert_eq!(&p1.hash, &p2.hash);
    }

    #[test]
    fn fuzz_insert_delete() {
        let mut rng = rand::thread_rng();
        let mut data = generate_random_data(1000, 8);

        let mut patricia = Patricia::new();
        for item in &data {
            patricia.insert(&item.0, item.1);
        }
        rng.shuffle(&mut data);
        for item in &data {
            let v = patricia.get(&item.0);
            assert_eq!(v, Some(&item.1));
        }

        rng.shuffle(&mut data);
        for item in &data {
            let v = patricia.remove(&item.0);
            assert_eq!(v, Some(item.1));
        }

        assert!(patricia.is_empty());
        validate_patricia(&patricia);
    }

    #[test]
    fn fuzz_hash_eq() {
        let mut rng = rand::thread_rng();
        let mut data = generate_random_data(400, 4);

        let mut p1 = Patricia::new();
        for item in &data {
            p1.insert(&item.0, item.1);
        }
        rng.shuffle(&mut data);
        let mut p2 = Patricia::new();
        for item in &data {
            p2.insert(&item.0, item.1);
        }

        validate_patricia(&p1);
        validate_patricia(&p2);
        assert_eq!(&p1.hash, &p2.hash);
    }

    #[test]
    fn fuzz_hash_eq_after_remove() {
        let mut rng = rand::thread_rng();
        let mut data = generate_random_data(300, 3);

        let mut keys_to_remove = data.iter()
            .take(270)
            .map(|item| item.0.clone())
            .collect::<Vec<_>>();

        let mut p1 = Patricia::new();
        for item in &data {
            p1.insert(&item.0, item.1);
        }

        rng.shuffle(&mut data);
        let mut p2 = Patricia::new();
        for item in &data {
            p2.insert(&item.0, item.1);
        }

        for key in &keys_to_remove {
            p1.remove(key);
        }
        rng.shuffle(&mut keys_to_remove);
        for key in &keys_to_remove {
            p2.remove(key);
        }

        validate_patricia(&p1);
        validate_patricia(&p2);

        println!("p1 after remove: {:#?}", p1);
        println!("p2 after remove: {:#?}", p2);

        assert_eq!(&p1.hash, &p2.hash);
    }


}
