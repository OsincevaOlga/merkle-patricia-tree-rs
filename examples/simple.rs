extern crate patricia;

use patricia::patricia::Patricia;

fn main() {
    let mut patricia = Patricia::new();
    patricia.insert(b"aba", 150);
    patricia.insert(b"caba", 250);
    patricia.insert(b"abacaba", 300);

    println!("abacaba is {}", patricia.get(b"abacaba").unwrap());

    let mut roman = Patricia::new();
    roman.insert(b"abacaba", 300);
    roman.insert(b"caba", 250);
    roman.insert(b"aba", 150);

    if patricia == roman {
        println!("Roman is just like Patricia");
    }
}
